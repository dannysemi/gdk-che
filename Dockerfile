FROM eclipse/stack-base:ubuntu
LABEL authors.maintainer "Grzegorz Bizon <grzegorz@gitlab.com>"
LABEL authors.contributor "Hrvoje Marjanovic <hrvoje.marjanovic@gmail.com>"

ENV DEBIAN_FRONTEND noninteractive

USER root
# install essentials

RUN apt-get update
RUN apt-get -y install build-essential \
                       software-properties-common \
                       python-software-properties

# rest of gitlab requirements
RUN apt-get install -y git postgresql postgresql-contrib libpq-dev \
                       redis-server libicu-dev cmake g++ libkrb5-dev libre2-dev \
                       ed pkg-config libsqlite3-dev libreadline-dev libssl-dev


# install nodejs
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs

# GDK tools
RUN apt-get install -y net-tools psmisc apt-transport-https

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install -y yarn

# install Go
RUN wget -q https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz
RUN tar -C /usr/local -xzf go1.8.3.linux-amd64.tar.gz
ENV PATH $PATH:/usr/local/go/bin

USER user

# Install rbenv
RUN git clone https://github.com/sstephenson/rbenv.git /home/user/.rbenv
RUN echo 'export PATH="/home/user/.rbenv/libexec:$PATH"' >> /home/user/.bash_profile
RUN echo 'eval "$(rbenv init -)"' >> /home/user/.bash_profile
ENV CONFIGURE_OPTS --disable-install-doc

# install ruby-build
RUN mkdir /home/user/.rbenv/plugins
RUN git clone https://github.com/sstephenson/ruby-build.git /home/user/.rbenv/plugins/ruby-build

RUN bash -l -c "rbenv install 2.3.6 && rbenv global 2.3.6"
